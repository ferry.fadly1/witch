package com.geekseat.ferry.test.testjava.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Ferry Fadly
 */
@Setter
@Getter
public class WitchVictimDto {

	private Long id;

	private String name;

	private int ageOfDeath;

	private int yearOfDeath;

}
