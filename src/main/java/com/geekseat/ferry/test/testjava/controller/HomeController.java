package com.geekseat.ferry.test.testjava.controller;

import java.util.ArrayList;
import java.util.List;

import com.geekseat.ferry.test.testjava.dto.WitchDto;
import com.geekseat.ferry.test.testjava.dto.WitchVictimDto;
import com.geekseat.ferry.test.testjava.util.Util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Ferry Fadly
 */
@Controller
public class HomeController {

	@Autowired
	Util util;

	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.GET)
	public ModelAndView home(WitchDto witchDto) {

		ModelAndView modelAndView = new ModelAndView();

		// Page Title
		modelAndView.addObject("title", "Welcome | Lets Burn The Witch !");

		// route page
		modelAndView.setViewName("index");

		modelAndView.addObject("witchDto", new WitchDto());

		return modelAndView;
	}

	@RequestMapping(value = { "/", "/home" }, method = RequestMethod.POST)
	public ModelAndView homePost(@ModelAttribute WitchDto witchDto, BindingResult bindingResult) {

		ModelAndView modelAndView = new ModelAndView();

		List<Double> sumList = new ArrayList<>();

		int inputErrorReturn = 0;

		for (WitchVictimDto wvd : witchDto.getWitchVictimDto()) {
			if ((wvd.getAgeOfDeath() >= wvd.getYearOfDeath()) || wvd.getAgeOfDeath() < 0) {
				inputErrorReturn = -1;
				break;
			}
			sumList.add( (double) util.calculateSum(wvd.getAgeOfDeath(), wvd.getYearOfDeath()));
		}

		double totalSum = sumList.stream().mapToDouble(Double::doubleValue).sum();
		double avg = totalSum / witchDto.getWitchVictimDto().size();

		if (inputErrorReturn < 0) {
			modelAndView.addObject("avgResult", inputErrorReturn);
		} else {
			modelAndView.addObject("avgResult", avg);
		}

		// Page Title
		modelAndView.addObject("title", "Welcome | Lets Burn The Witch !");

		// route page
		modelAndView.setViewName("redirect:/home");

		return modelAndView;
	}

}
