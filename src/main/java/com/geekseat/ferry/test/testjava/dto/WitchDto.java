package com.geekseat.ferry.test.testjava.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Ferry Fadly
 */
@Setter
@Getter
public class WitchDto {

	private Long id;
	
	private String witchName;

	private String witchLocation;
	
	private List<WitchVictimDto> witchVictimDto;
	
}
