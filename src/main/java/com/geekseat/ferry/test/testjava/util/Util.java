package com.geekseat.ferry.test.testjava.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

/**
 *
 * @author Ferry Fadly
 */
@Service("util")
public class Util {

	public Integer calculateSum(int ageOfDeath, int yearOfDeath) {

		int n0 = 0;
		int n1 = 1;
		int n2 = 0;
		int total = 1;

		for (int x = 2; x <= (yearOfDeath - ageOfDeath); x++) {
			n2 = n0 + n1;
			total = total + n2;
			n0 = n1;
			n1 = n2;
		}

		return total;
	}

}
