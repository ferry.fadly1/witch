package com.geekseat.ferry.test.testjava;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Ferry Fadly
 * 
 */
@SpringBootApplication
public class TestjavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestjavaApplication.class, args);

		// test the algorithm
		// System.out.println("avg = "+ (calculateSum(10,12) +calculateSum(13,17)) / 2);

	}

	static Integer calculateSum(int ageOfDeath, int yearOfDeath) {

		int n0 = 0;
		int n1 = 1;
		int n2 = 0;
		int total = 1;

		for (int x = 2; x <= (yearOfDeath - ageOfDeath); x++) {
			n2 = n0 + n1;
			total = total + n2;
			n0 = n1;
			n1 = n2;
		}

		return total;
	}

}
