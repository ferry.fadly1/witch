package com.geekseat.ferry.test.testjava;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import com.geekseat.ferry.test.testjava.util.Util;

@SpringBootTest
@AutoConfigureMockMvc
class TestjavaApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private Util util;

	@Test
	void home() throws Exception {
		mockMvc.perform(get("/home")).andExpect(status().isOk());
	}

	@Test
	void root() throws Exception {
		mockMvc.perform(get("/")).andExpect(status().isOk());
	}

	@Test
	void calculate() throws Exception {

		double avg = (double) (util.calculateSum(10, 12) + util.calculateSum(13, 17)) / 2;

		assertEquals(4.5, avg);
	}

	@Test
	void testVersiAwal() throws Exception {

		double avg = (double) (TestjavaApplication.calculateSum(10, 12) + TestjavaApplication.calculateSum(13, 17)) / 2;

		assertEquals(4.5, avg);
	}

	@Test
	void testPostUserviaWeb() throws Exception {

		this.mockMvc.perform(post("/")
				.param("id", "1")
				.param("witchName", "Penyihir Sakti")
				.param("witchLocation", "Bandung")
				.param("witchVictimDto[0].id", "1")
				.param("witchVictimDto[0].name", "Ferry")
				.param("witchVictimDto[0].ageOfDeath", "10")
				.param("witchVictimDto[0].yearOfDeath", "12")
				.param("witchVictimDto[1].id", "2")
				.param("witchVictimDto[1].name", "fadly")
				.param("witchVictimDto[1].ageOfDeath", "13")
				.param("witchVictimDto[1].yearOfDeath", "17"))
				.andExpect(redirectedUrl("/home?avgResult=4.5&title=Welcome+%7C+Lets+Burn+The+Witch+%21"));
	}

	@Test
	void testPostUserviaWebNegativeValue() throws Exception {

		this.mockMvc.perform(post("/")
				.param("id", "1")
				.param("witchName", "Penyihir Sakti")
				.param("witchLocation", "Bandung")
				.param("witchVictimDto[0].id", "1")
				.param("witchVictimDto[0].name", "Ferry")
				.param("witchVictimDto[0].ageOfDeath", "-10")
				.param("witchVictimDto[0].yearOfDeath", "-12")
				.param("witchVictimDto[1].id", "2")
				.param("witchVictimDto[1].name", "fadly")
				.param("witchVictimDto[1].ageOfDeath", "-13")
				.param("witchVictimDto[1].yearOfDeath", "-17"))
				.andExpect(redirectedUrl("/home?avgResult=-1&title=Welcome+%7C+Lets+Burn+The+Witch+%21"));
	}

	@Test
	void main() {
		TestjavaApplication.main(new String[] {});
	}

}
